var express =require("express");
var router=express.Router();
var biciController=require('../../controller/api/bicicletaController');

router.get('/',biciController.bicicleta_list);
router.post('/create',biciController.bicicleta_create);
router.post('/delete',biciController.bicicleta_delete);
module.exports=router;