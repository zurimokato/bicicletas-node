var Bicicleta =require('../../models/bicicleta');


exports.bicicleta_list=function(req,res){
    res.status(200).json({
        bicicletas:Bicicleta.allBicis
    })
}

exports.bicicleta_create=function(req,res){
    var bicicleta=new Bicicleta(req.body.id,req.body.color,req.body.modelo);
    bicicleta.ubicacion=[req.body.lat,req.body.lng];

    Bicicleta.add(bicicleta);
    
    res.status(200).json({
        bicicleta:bicicleta
    })
}

exports.bicicleta_delete=function(req,res){
    var bici=Bicicleta.removeById(req.body.id);
    res.status(204).send();
}